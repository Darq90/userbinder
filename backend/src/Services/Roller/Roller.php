<?php

declare(strict_types=1);

namespace App\Services\Roller;


use App\Core\ValueObjects\RollerConfig;

class Roller
{
    public function roll(RollerConfig $rollerConfig): ?string
    {
        $combinations = array_map(static fn($value) => ++$value, $rollerConfig->getCombinations());
        $processResult = false;

        try {
            do {
                $key = "";
                $combinationsSum = array_sum($combinations);
                $results = self::makeRollsTab(self::makeRollsValuesTab($combinationsSum), $combinations);
                $rand = random_int(0, ($combinationsSum - 1));

                foreach ($results as $resultKey => $resultValue) {
                    if (in_array($rand, $resultValue, true)) {
                        $key = $resultKey;

                        break;
                    }
                }

                if (empty($key)) {
                    return null;
                }

                unset($combinations[$key]);

                if (count($combinations) <= 1) {
                    $processResult = array_keys($combinations)[0];
                }

            } while ($processResult === false);

            return $processResult;

        } catch (\Exception $e) {
            return null;
        }
    }

    private static function makeRollsValuesTab($combinationsSum): array
    {
        $rollsTab = [];
        for ($i = 0; $i < $combinationsSum; $i++) {
            $rollsTab[] = $i;
        }

        return $rollsTab;
    }

    private static function makeRollsTab(array $rollTab, array $combinations): array
    {
        $results = [];
        foreach ($combinations as $combinationKey => $combinationValue) {
            for ($i = 0; $i < $combinationValue; $i++) {
                try {
                    $rand = random_int(0, (count($rollTab) - 1));
                    $results[$combinationKey][] = $rollTab[$rand];

                    unset($rollTab[$rand]);
                    $rollTab = array_values($rollTab);

                } catch (\Exception $e) {
                    return [];
                }
            }
        }

        return $results;
    }
}