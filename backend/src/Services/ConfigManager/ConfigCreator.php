<?php

declare(strict_types=1);

namespace App\Services\ConfigManager;


use App\Core\ValueObjects\Team;
use App\Utils\FileManager\FileManager;

class ConfigCreator extends Manager
{
    public function createEmptyConfig(Team $team): ?string
    {
        try {
            do {
                $randomEmptyFileName = md5((string)random_int(0, 1000000000))."_".date("YmdHi");
            } while (FileManager::isFileExists($this->temporaryResourcesPath, $randomEmptyFileName));

            $file = FileManager::createFile($this->temporaryResourcesPath, $randomEmptyFileName)
                ->saveToFile(
                    $this->generateHashMessage(json_encode(self::generateEmptyConfig($team), JSON_THROW_ON_ERROR))
                );

        } catch (\JsonException | \Exception $e) {

            return null;
        }

        return $file->getFilePath();
    }
}