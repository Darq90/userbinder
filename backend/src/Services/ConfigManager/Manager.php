<?php

declare(strict_types=1);

namespace App\Services\ConfigManager;

use App\Core\ValueObjects\RollerConfig;
use App\Core\ValueObjects\Team;

class Manager
{
    private const HASH_METHOD = "AES-128-ECB";
    protected string $temporaryResourcesPath;
    private string $hashPassword;

    public function __construct(
        $temporaryResourcesPath,
        $hashPassword
    )
    {
        $this->temporaryResourcesPath = $temporaryResourcesPath;
        $this->hashPassword = $hashPassword;
    }

    public function changeResourcePath(string $path): self
    {
        $this->temporaryResourcesPath = $path;

        return $this;
    }

    protected static function generateEmptyConfig(Team $team): RollerConfig
    {
        return RollerConfig::createEmptyConfigFromTeam($team);
    }

    protected function generateHashMessage(string $message): string
    {
        //todo : change after
        return $message;
//        return openssl_encrypt($message,self::HASH_METHOD, $this->hashPassword);
    }

    protected function loadHashMessage(string $message): string
    {
        //todo : change after
        return $message;
//        return openssl_decrypt($message,self::HASH_METHOD, $this->hashPassword);
    }
}