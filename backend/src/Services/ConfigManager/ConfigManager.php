<?php

declare(strict_types=1);

namespace App\Services\ConfigManager;


use App\Core\ValueObjects\RollerConfig;
use App\Utils\FileManager\FileManager;

class ConfigManager extends Manager
{
    public function loadConfig(string $name, string $resourceDir = ""): ?RollerConfig
    {
        if (!empty($resourceDir)) {
            $this->temporaryResourcesPath = $resourceDir;
        }

        if (!is_file($filePath = $this->temporaryResourcesPath.$name)) {
            return null;
        }

        $config = $this->load($filePath);
        if ($config === null) {
            return null;
        }

        return $config;
    }

    public function saveConfig(string $name, RollerConfig $rollerConfig): bool
    {
        if (!is_file($filePath = $this->temporaryResourcesPath.$name)) {
            return false;
        }

        $file = FileManager::loadFile($filePath);

        try {
            $file->saveToFile(json_encode($rollerConfig,  JSON_THROW_ON_ERROR));

            return true;
        } catch (\JsonException $e) {

            return false;
        }
    }

    private function load($filePath): RollerConfig
    {
        $file = FileManager::loadFile($filePath);

        return RollerConfig::createFromJson($this->loadHashMessage($file->getFromFile()), $filePath);
    }
}