<?php


namespace App\Helpers\TeamHelper;


class TeamManagerHelper
{
    public static function divideForTeams(
        string $combinationKey,
        string $separator = "_"): object
    {
        $teams = explode($separator, $combinationKey);
        $team1 = $team2 = [];

        $teamCounter = count($teams);
        $counter = ($teamCounter / 2);

        foreach ($teams as $person) {
            if ($counter > 0) {
                $team1[] = $person;
            } else {
                $team2[] = $person;
            }

            --$counter;
        }

        shuffle($team1);
        shuffle($team2);

        return (object)[
            'team1' => $team1,
            'team2' => $team2
        ];
    }
}