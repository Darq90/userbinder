<?php


namespace App\Helpers\UserHelper;


class UserHelper
{
    private string $sfUser;
    private string $sfPassword;
    private string $sfConfig;

    public function __construct($sfUser, $sfPassword, $sfConfig)
    {
        $this->sfUser = $sfUser;
        $this->sfPassword = $sfPassword;
        $this->sfConfig = $sfConfig;
    }

    public function getSfUser(): string
    {
        return $this->sfUser;
    }

    public function getSfPassword(): string
    {
        return $this->sfPassword;
    }

    public function getSfConfig(): string
    {
        return $this->sfConfig;
    }
}