<?php


namespace App\Utils\FileManager;


use App\Exceptions\FileExistsException;
use App\Exceptions\FileNotExistsException;

class FileManager
{
    private $filePath;

    protected function __construct(string $pathToFile)
    {
        $this->filePath = $pathToFile;
    }

    public static function loadFile(string $pathToFile): FileManager
    {
        if (!is_file($pathToFile)) {
            throw FileNotExistsException::create(
                self::class,
                "Can't load: ${pathToFile}"
            );
        }

        return new self($pathToFile);
    }

    public static function createFile(string $directory, string $fileName): FileManager
    {
        if (self::isFileExists($directory, $fileName)) {
            throw FileExistsException::create(
                self::class,
                "File: ${fileName} exists"
            );
        }

        return new self(
            self::mergeDirectoryAndFileName($directory, $fileName)
        );
    }

    public static function isFileExists(string $directory, string $fileName): bool
    {
        return is_file(
            self::mergeDirectoryAndFileName($directory, $fileName)
        );
    }

    public function saveToFile(string $line): self
    {
        file_put_contents($this->filePath, $line);

        return $this;
    }

    public function getFromFile(): ?string
    {
        if (is_file($this->getFilePath())) {
            return file_get_contents($this->getFilePath());
        }

        return null;
    }

    private static function mergeDirectoryAndFileName(string $directory, string $fileName): string
    {
        if (substr($directory, -1) !== "/") {
            $directory .= "/";
        }

         return $directory.$fileName;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }
}