<?php

declare(strict_types=1);

namespace App\Messenger\Messages;

use App\Core\ValueObjects\RollerConfig;
use App\Core\ValueObjects\Status;


class Roll
{
    private Status $status;
    private int $games;
    private RollerConfig $rollerConfig;
    private int $rolls;
    private array $results;

    public function __construct(
        RollerConfig $rollerConfig,
        int $rolls = 1,
        int $games = 1
    )
    {
        $this->rollerConfig = $rollerConfig;
        $this->rolls = $rolls;
        $this->games = $games;
        $this->results = [];
        $this->status = Status::createPendingStatus();
    }

    public function getGames(): int
    {
        return $this->games;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }

    public function getRollerConfig(): RollerConfig
    {
        return $this->rollerConfig;
    }

    public function getRolls(): int
    {
        return $this->rolls;
    }

    public function getResults(): array
    {
        return $this->results;
    }

    public function setResults(array $results): self
    {
        $this->results = $results;

        return $this;
    }

    public function setRollerConfig(RollerConfig $rollerConfig): self
    {
        $this->rollerConfig = $rollerConfig;

        return $this;
    }
}