<?php

declare(strict_types=1);

namespace App\Messenger\Messages;


use App\Core\ValueObjects\Status;

class CreateEmptyConfig
{
    private array $temporaryPlayers;
    private Status $status;
    private string $configPath;

    public function __construct(array $temporaryPlayers, string $configPath = "")
    {
        $this->temporaryPlayers = $temporaryPlayers;
        $this->configPath = $configPath;
        $this->status = Status::createPendingStatus();
    }

    public function getTemporaryPlayers(): array
    {
        return $this->temporaryPlayers;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getConfigPath(): string
    {
        return $this->configPath;
    }

    public function setConfigPath(string $configPath): self
    {
        $this->configPath = $configPath;

        return $this;
    }
}