<?php

declare(strict_types=1);

namespace App\Messenger\Messages;



use App\Core\ValueObjects\RollerConfig;

class EditConfig
{
    private RollerConfig $rollerConfig;

    public function __construct(RollerConfig $rollerConfig)
    {
        $this->rollerConfig = $rollerConfig;
    }

    public function getRollerConfig(): RollerConfig
    {
        return $this->rollerConfig;
    }
}