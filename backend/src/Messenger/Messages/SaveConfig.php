<?php

declare(strict_types=1);

namespace App\Messenger\Messages;

use App\Core\ValueObjects\RollerConfig;
use App\Core\ValueObjects\Status;


class SaveConfig
{
    private string $name;
    private RollerConfig $rollerConfig;
    private Status $status;

    public function __construct(
        string $name,
        RollerConfig $rollerConfig
    )
    {
        $this->name = $name;
        $this->rollerConfig = $rollerConfig;
        $this->status = Status::createPendingStatus();
    }
    public function getName(): string
    {
        return $this->name;
    }

    public function getRollerConfig(): RollerConfig
    {
        return $this->rollerConfig;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }
}