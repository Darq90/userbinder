<?php

declare(strict_types=1);

namespace App\Messenger\Messages;

use App\Core\ValueObjects\RollerConfig;
use App\Core\ValueObjects\Status;


class LoadConfig
{
    private string $name;
    private Status $status;
    private ?RollerConfig $rollerConfig;
    private string $resourceDir;

    public function __construct(string $name, string $resourceDir = "")
    {
        $this->name = $name;
        $this->rollerConfig = null;
        $this->status = Status::createPendingStatus();
        $this->resourceDir = $resourceDir;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRollerConfig(): ?RollerConfig
    {
        return $this->rollerConfig;
    }

    public function setRollerConfig(RollerConfig $rollerConfig): self
    {
        $this->rollerConfig = $rollerConfig;

        return $this;
    }

    public function getResourceDir(): string
    {
        return $this->resourceDir;
    }
}