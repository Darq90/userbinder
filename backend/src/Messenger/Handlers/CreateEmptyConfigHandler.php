<?php

declare(strict_types=1);

namespace App\Messenger\Handlers;


use App\Core\ValueObjects\Player;
use App\Core\ValueObjects\Status;
use App\Core\ValueObjects\Team;
use App\Exceptions\BadDataException;
use App\Messenger\Messages\CreateEmptyConfig;
use App\Services\ConfigManager\ConfigCreator;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateEmptyConfigHandler implements MessageHandlerInterface
{
    private ConfigCreator $configCreator;

    public function __construct(
        ConfigCreator $configCreator
    )
    {
        $this->configCreator = $configCreator;
    }

    public function __invoke(CreateEmptyConfig $createEmptyConfig)
    {
        $players = [];

        if (!empty($createEmptyConfig->getConfigPath())) {
            $this->configCreator->changeResourcePath(
                $createEmptyConfig->getConfigPath()
            );
        }

        try {
            foreach ($createEmptyConfig->getTemporaryPlayers() as $player) {
                $players[] = Player::create($player);
            }

            $configPath = $this->configCreator->createEmptyConfig(Team::create($players));

            if ($configPath !== NULL) {
                $createEmptyConfig
                    ->setConfigPath($configPath)
                    ->setStatus(Status::createSuccessStatus());

                return true;
            }
        } catch (BadDataException $exception) {
            //todo : add loging error
        }

        $createEmptyConfig->setStatus(Status::createFailureStatus());
    }
}