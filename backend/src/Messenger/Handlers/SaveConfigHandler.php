<?php

declare(strict_types=1);

namespace App\Messenger\Handlers;

use App\Core\ValueObjects\Status;
use App\Messenger\Messages\SaveConfig;
use App\Services\ConfigManager\ConfigManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SaveConfigHandler implements MessageHandlerInterface
{
    private ConfigManager $configManager;

    public function __construct(
        ConfigManager $configManager
    )
    {
        $this->configManager = $configManager;
    }

    public function __invoke(SaveConfig $saveConfig)
    {
        if ($this->configManager->saveConfig($saveConfig->getName(), $saveConfig->getRollerConfig())) {
            $saveConfig
                ->setStatus(Status::createSuccessStatus());

            return true;
        }

        $saveConfig->setStatus(Status::createFailureStatus());
    }
}