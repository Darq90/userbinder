<?php

declare(strict_types=1);

namespace App\Messenger\Handlers;


use App\Messenger\Messages\EditConfig;
use App\Services\ConfigManager\ConfigCreator;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EditConfigHandler implements MessageHandlerInterface
{
    private ConfigCreator $configCreator;

    public function __construct(
        ConfigCreator $configCreator
    )
    {
        $this->configCreator = $configCreator;
    }

    public function __invoke(EditConfig $editConfig)
    {

    }
}