<?php

declare(strict_types=1);

namespace App\Messenger\Handlers;

use App\Core\ValueObjects\Status;
use App\Exceptions\BadDataException;
use App\Messenger\Messages\LoadConfig;
use App\Services\ConfigManager\ConfigManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class LoadConfigHandler implements MessageHandlerInterface
{
    private ConfigManager $configManager;

    public function __construct(
        ConfigManager $configManager
    )
    {
        $this->configManager = $configManager;
    }

    public function __invoke(LoadConfig $loadConfig)
    {
        $config = $this->configManager->loadConfig($loadConfig->getName(), $loadConfig->getResourceDir());

        if ($config !== null) {
            $loadConfig
                ->setStatus(Status::createSuccessStatus())
                ->setRollerConfig($config);
            return true;
        }

        $loadConfig->setStatus(Status::createFailureStatus());
    }
}