<?php

declare(strict_types=1);

namespace App\Messenger\Handlers;

use App\Core\ValueObjects\RollerConfig;
use App\Core\ValueObjects\Status;
use App\Messenger\Messages\Roll;
use App\Services\Roller\Roller;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RollHandler implements MessageHandlerInterface
{
    private Roller $roller;

    public function __construct(
        Roller $roller
    )
    {
        $this->roller = $roller;
    }

    public function __invoke(Roll $roll)
    {
        $rolls = [];

        for ($i = 0; $i < $roll->getRolls(); $i++) {
            $rolls[] = $this->roller->roll($roll->getRollerConfig());
        }

        $combinations = $roll->getRollerConfig()->getCombinations();
        $lastKey = end($rolls);

        if (!isset($combinations[$lastKey])) {
            $roll->setStatus(Status::createFailureStatus());

            return false;
        }

        $combinations[$lastKey] += $roll->getGames();

        $newConfig = RollerConfig::createFromArray($combinations, $roll->getRollerConfig()->getTeamCount());
        if ($newConfig === null) {
            $roll->setStatus(Status::createFailureStatus());

            return false;
        }

        $roll
            ->setRollerConfig($newConfig)
            ->setResults($rolls)
            ->setStatus(Status::createSuccessStatus());
    }
}