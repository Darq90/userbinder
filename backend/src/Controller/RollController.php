<?php

namespace App\Controller;

use App\Core\FrameworkHelpers\HelperAbstractController;
use App\Core\ValueObjects\Status;
use App\Helpers\TeamHelper\TeamManagerHelper;
use App\Messenger\Messages\LoadConfig;
use App\Messenger\Messages\Roll;
use App\Messenger\Messages\SaveConfig;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('api/roll')]
class RollController extends HelperAbstractController
{
    private const ROLLS = 1000;

    #[Route('', name: 'roll', methods: ['POST'])]
    public function index(Request $request): Response
    {
        try {
            if (empty($body = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR))) {
                throw new \Exception("empty content");
            }

            if (empty($body['config'])) {
                throw new \Exception("empty config");
            }

        } catch (\JsonException | \Exception $e) {
            return $this->json([
                "error" => true
            ], 400);
        }

        $configMessage = new LoadConfig(
            $body['config'],
            $this->getResourcePath()
        );

        $this->bus->dispatch($configMessage);

        if ($configMessage->getStatus()->getStatus() !== Status::SUCCESS) {
            return $this->json([
                "error" => true
            ], 400);
        }

        $rolls = (!empty($body["rolls"]) ? (int) $body["rolls"] : 1);
        $games = (!empty($body["games"]) ? (int) $body["games"] : 1);

        $message = new Roll(
            $configMessage->getRollerConfig(),
            $rolls,
            $games
        );

        $this->bus->dispatch($message);

        if ($message->getStatus()->getStatus() !== Status::SUCCESS) {
            return $this->json([
                "error" => true
            ], 400);
        }

        $rollResults = $lastResults = [];
        foreach ($message->getResults() as $roll => $result) {
            $team = TeamManagerHelper::divideForTeams($result);

            $lastResults = [
                'team1' => implode(", ", $team->team1),
                'team2' => implode(", ", $team->team2)
            ];

            $rollResults[] = $lastResults;
        }

        $saveMessage = new SaveConfig(
            $body['config'],
            $message->getRollerConfig()
        );

        $this->bus->dispatch($saveMessage);
        if ($saveMessage->getStatus()->getStatus() !== Status::SUCCESS) {
            return $this->json([
                "error" => true
            ], 400);
        }

        return $this->json([
            'rolls' => $rollResults,
            'finalResults' => $lastResults
        ]);
    }

    #[Route('/probabilityDistribution', name: 'probabilityDistribution', methods: ['POST'])]
    public function probabilityDistribution(Request $request): Response
    {
        try {
            if (empty($body = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR))) {
                throw new \Exception("empty content");
            }

            if (empty($body['config'])) {
                throw new \Exception("empty config");
            }

        } catch (\JsonException | \Exception $e) {
            return $this->json([
                "error" => true
            ], 400);
        }

        $configMessage = new LoadConfig(
            $body['config'],
            $this->getResourcePath()
        );

        $this->bus->dispatch($configMessage);

        if ($configMessage->getStatus()->getStatus() !== Status::SUCCESS) {
            return $this->json([
                "error" => true
            ], 400);
        }


        $message = new Roll(
            $configMessage->getRollerConfig(),
            self::ROLLS,
            1
        );

        $this->bus->dispatch($message);

        if ($message->getStatus()->getStatus() !== Status::SUCCESS) {
            return $this->json([
                "error" => true
            ], 400);
        }

        $tmpResults = $finalResults = [];

        foreach ($message->getResults() as $roll => $result) {
            if (!isset($tmpResults[$result])) {
                $tmpResults[$result] = 0;
            }

            ++$tmpResults[$result];
        }

        if ($configMessage->getRollerConfig() !== null && $configMessage->getRollerConfig()->getCombinations() !== null) {
            foreach ($configMessage->getRollerConfig()->getCombinations() as $combinationKey => $combinationValue) {
                if (!isset($tmpResults[$combinationKey])) {
                    $tmpResults[$combinationKey] = 0.01;
                }
            }
        }

        foreach ($tmpResults as $key => $value) {
            $team = TeamManagerHelper::divideForTeams($key);

            $finalResults[] = [
                'team1' => implode(", ", $team->team1),
                'team2' => implode(", ", $team->team2),
                'count' => (($value / self::ROLLS) * 100)." %"
            ];
        }

        return $this->json([
            'results' => $finalResults
        ]);
    }
}
