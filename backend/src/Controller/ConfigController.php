<?php

namespace App\Controller;

use App\Core\FrameworkHelpers\HelperAbstractController;
use App\Core\ValueObjects\Status;
use App\Helpers\TeamHelper\TeamManagerHelper;
use App\Messenger\Messages\CreateEmptyConfig;
use App\Messenger\Messages\LoadConfig;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('api/config')]
class ConfigController extends HelperAbstractController
{
    #[Route('/createNewConfig', name: 'createNewConfig', methods: ["POST"])]
    public function createNewConfig(Request $request): JsonResponse
    {
        try {
            if (empty($players = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR))) {
                throw new \Exception("empty players");
            }

        } catch (\JsonException $e) {
            return $this->json([
                "error" => true
            ], 400);
        }

        if (count($players) < 4) {
            return $this->json([
                "error" => true
            ]);
        }

        $message = new CreateEmptyConfig($players, $this->getResourcePath());
        $this->bus->dispatch($message);

        if ($message->getStatus()->getStatus() !== Status::SUCCESS) {
            return $this->json([
                "error" => true
            ], 400);
        }

        return $this->json([
            'results' => substr($message->getConfigPath(), (strrpos($message->getConfigPath(), "/") + 1))
        ]);
    }


    #[Route('/{configName}', name: 'loadConfig', methods: ["GET"])]
    public function loadConfig(string $configName): JsonResponse
    {
        if (empty($configName)) {
            return $this->json([
               "error" => true
            ], 400);
        }

        $message = new LoadConfig(
            $configName,
            $this->getResourcePath()
        );

        $this->bus->dispatch($message);

        if (($message->getStatus()->getStatus() !== Status::SUCCESS) || ($message->getRollerConfig() === null)) {
            return $this->json([
                "error" => true
            ], 400);
        }

        $configTable = [];
        foreach ($message->getRollerConfig()->getCombinations() as $key => $value) {
            $team = TeamManagerHelper::divideForTeams($key);

            $configTable[] = [
                'team1' => implode(", ", $team->team1),
                'team2' => implode(", ", $team->team2),
                'count' => $value
            ];
        }

        return $this->json([
            'results' => $configTable
        ]);
    }
}
