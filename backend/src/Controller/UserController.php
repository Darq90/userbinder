<?php


namespace App\Controller;


use App\Core\FrameworkHelpers\HelperAbstractController;
use App\Exceptions\BadDataException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\User;

#[Route('api/user')]
class UserController extends HelperAbstractController
{
    #[Route('/login', name: 'login', methods: ['POST'])]
    public function login(Request $request): Response
    {
        try {
            $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

            if (empty($content['login']) || empty($content['password'])) {
                throw BadDataException::create(self::class, 'bad login or password');
            }

            $user = $this->generateUser($content['login'], $content['password']);

            if ($user === null) {
                throw BadDataException::create(self::class, 'bad login or password');
            }

        } catch (\JsonException | BadDataException $e) {
            return $this->json([
                "error" => true
            ], 400);
        }

        return $this->json([
            'configName' => $this->getConfig()
        ]);
    }
}