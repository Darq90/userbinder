<?php

declare(strict_types=1);

namespace App\Core\ValueObjects;


use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class RollerConfig extends ValueObject
{
    private array $combinations;
    private int $teamCount;
    private string $configFilePath;

    protected function __construct(array $combinations, int $teamCount, string $configFilePath = "")
    {
        $this->combinations = $combinations;
        $this->teamCount = $teamCount;
        $this->configFilePath = $configFilePath;
    }

    public static function createFromJson($json, $configFilePath): ?RollerConfig
    {
        $json = json_decode($json, true);
        if (empty($json["teamCount"] || empty($json["combinations"]))) {
            return null;
        }

        return new self($json["combinations"], $json["teamCount"], $configFilePath);
    }

    public static function createFromArray(array $combinations, int $teams, string $configFilePath = ""): ?RollerConfig
    {
        return new self($combinations, $teams, $configFilePath);
    }

    public static function createEmptyConfigFromTeam(Team $team): RollerConfig
    {
        $teams = 2;

        //todo : add more teams here
        return new self(
            self::prepareEmptyTeamsStats($team),
            $teams
        );
    }

    private static function prepareEmptyTeamsStats(Team $team): array
    {
        $results = $starterTab = [];
        for ($i = 0; $i < $team->getTeamCount(); $i++) {
            $starterTab[] = $i;
        }

        for ($i = 1; $i < $team->getTeamCount() - 1; $i++) {
            $tmpTeam = [];

            for ($j = $i + 1; $j <= $team->getTeamCount() - 1; $j++) {
                $tmpStarter = $starterTab;
                $tmp = [
                    $team->getPlayerNameById(0),
                    $team->getPlayerNameById($i)
                ];

                unset($tmpStarter[0], $tmpStarter[$i]);

                for($k = $j; $k <= $team->getTeamCount() - 1; $k++) {
                    $tmp[] = $team->getPlayerNameById($k);
                    unset($tmpStarter[$k]);
                }

                foreach ($tmpStarter as $id) {
                    $tmp[] = $team->getPlayerNameById($id);
                }

                $tmpTeam[] = implode("_", $tmp);
            }

            foreach ($tmpTeam as $singleResult) {
                $results[$singleResult] = 0;
            }
        }

        return $results;
    }

    public function getConfigFilePath(): string
    {
        return $this->configFilePath;
    }

    public function setConfigFilePath(string $configFilePath): void
    {
        $this->configFilePath = $configFilePath;
    }

    public function getCombinations(): array
    {
        return $this->combinations;
    }

    public function getTeamCount(): int
    {
        return $this->teamCount;
    }

    #[Pure] #[ArrayShape(["teamCount" => "int", "combinations" => "array"])] public function jsonSerialize(): array
    {
        return [
            "teamCount" => $this->getTeamCount(),
            "combinations" => $this->getCombinations()
        ];
    }
}