<?php

declare(strict_types=1);

namespace App\Core\ValueObjects;


use JetBrains\PhpStorm\Pure;

class Team extends ValueObject
{
    private array $team;

    protected function __construct(array $team)
    {
        $this->team = $team;
    }

    /**
     * @param Player[] $team
     * @return Team
     */
    #[Pure] public static function create(array $team): Team
    {
        return new self($team);
    }

    /**
     * @return Player[]
     */
    public function getTeam(): array
    {
        return $this->team;
    }

    #[Pure] public function getPlayersName(): array
    {
        $result = [];
        foreach ($this->getTeam() as $player) {
            $result[] = $player->getPlayerName();
        }

        return $result;
    }

    #[Pure] public function getPlayerNameById($id): ?string
    {
        if (!isset($this->team[$id])) {
            return null;
        }

        /** @var Player $player */
        $player = $this->team[$id];
        return $player->getPlayerName();
    }

    public function getTeamCount(): int
    {
        return count($this->team);
    }

    #[Pure] public function jsonSerialize(): array
    {
        return $this->getTeam();
    }
}