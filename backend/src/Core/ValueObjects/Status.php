<?php

declare(strict_types=1);

namespace App\Core\ValueObjects;


use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Status extends ValueObject
{
    public const PENDING = "PENDING";
    public const SUCCESS = "SUCCESS";
    public const FAILURE = "FAILURE";

    private string $status;

    protected function __construct($status)
    {
        $this->status = $status;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    #[Pure] public static function  createPendingStatus(): Status
    {
        return new Status(self::PENDING);
    }

    #[Pure] public static function  createSuccessStatus(): Status
    {
        return new Status(self::SUCCESS);
    }

    #[Pure] public static function  createFailureStatus(): Status
    {
        return new Status(self::FAILURE);
    }

    #[Pure] #[ArrayShape(['status' => "string"])] public function jsonSerialize(): array
    {
        return [
            'status' => $this->getStatus()
        ];
    }
}