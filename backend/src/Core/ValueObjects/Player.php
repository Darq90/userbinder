<?php

declare(strict_types=1);

namespace App\Core\ValueObjects;


use App\Exceptions\BadDataException;
use Assert\Assertion;
use Assert\AssertionFailedException;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Player extends ValueObject
{
    private string $playerName;

    protected function __construct(string $playerName)
    {
        $this->playerName = $playerName;
    }

    /**
     * @param string $playerName
     * @return Player
     * @throws BadDataException
     */
    public static function create(string $playerName): Player
    {
        if (self::validatePlayerName($playerName)) {
            return new self($playerName);
        }
    }

    /**
     * @param string $playerName
     * @return bool
     * @throws BadDataException
     */
    private static function validatePlayerName(string $playerName): bool
    {
        try {
            Assertion::notEmpty($playerName);
            Assertion::maxLength($playerName, 100);
            Assertion::regex($playerName, "/[\w\d]+/");
        } catch (AssertionFailedException $assertionFailedException) {
            throw BadDataException::create(
                self::class,
                $assertionFailedException->getMessage()
            );
        }

        return true;
    }

    public function getPlayerName(): string
    {
        return $this->playerName;
    }

    #[Pure] #[ArrayShape(["name" => "string"])] public function jsonSerialize(): array
    {
        return [
            "name" => $this->getPlayerName()
        ];
    }
}