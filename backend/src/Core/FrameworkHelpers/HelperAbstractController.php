<?php


namespace App\Core\FrameworkHelpers;


use App\Helpers\UserHelper\UserHelper;
use App\Security\UserAuthenticator;
use JetBrains\PhpStorm\Pure;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\User\User;

class HelperAbstractController extends AbstractController
{
    protected MessageBusInterface $bus;
    private string $webTemporaryResourcesPath;
    private UserAuthenticator $userAuthenticator;
    private UserHelper $userHelper;

    public function __construct(
        MessageBusInterface $bus,
        $webTemporaryResourcesPath,
        UserAuthenticator $userAuthenticator,
        UserHelper $userHelper
    )
    {
        $this->bus = $bus;
        $this->webTemporaryResourcesPath = $webTemporaryResourcesPath;
        $this->userAuthenticator = $userAuthenticator;
        $this->userHelper = $userHelper;
    }

    public function getResourcePath(): string
    {
        return $this->webTemporaryResourcesPath;
    }

    protected function generateUser(string $login, string $password): ?User
    {
        $user = new User($this->userHelper->getSfUser(), $this->userHelper->getSfPassword());
        if ($this->userAuthenticator->checkCredentials([$login, $password], $user)) {
            return $user;
        }

        return null;
    }

    #[Pure] protected function getConfig(): string
    {
        return $this->userHelper->getSfConfig();
    }
}