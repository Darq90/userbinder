<?php

declare(strict_types=1);

namespace App\Command;

use App\Core\ValueObjects\Status;
use App\Messenger\Messages\LoadConfig;
use App\Messenger\Messages\Roll;
use App\Messenger\Messages\SaveConfig;
use App\Services\Roller\Roller;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

class RollCommand extends Command
{
    protected static $defaultName = 'App:Roll';
    protected static $defaultDescription = 'Let\'s roll group';

    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();
        $this->bus = $bus;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('ConfigName', 'C', InputOption::VALUE_REQUIRED, 'config name')
            ->addOption('Rolls', 'R', InputOption::VALUE_OPTIONAL, 'roll times')
            ->addOption('Games', 'G', InputOption::VALUE_OPTIONAL, 'count games to save')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->info("config loading...");
        $configMessage = new LoadConfig(
            $input->getOption('ConfigName')
        );

        $this->bus->dispatch($configMessage);

        if ($configMessage->getStatus()->getStatus() !== Status::SUCCESS) {
            $io->error('Can\'t load config');

            return Command::FAILURE;
        }

        $rolls = (!empty($input->getOption("Rolls")) ? (int) $input->getOption("Rolls") : 1);
        $games = (!empty($input->getOption("Games")) ? (int) $input->getOption("Games") : 1);

        $message = new Roll(
            $configMessage->getRollerConfig(),
            $rolls,
            $games
        );

        $io->info("rolling...");
        $this->bus->dispatch($message);

        if ($message->getStatus()->getStatus() !== Status::SUCCESS) {
            $io->error('Problem on rolling');

            return Command::FAILURE;
        }

        $lastResult = "";

        foreach ($message->getResults() as $roll => $result) {
            $io->info("loading roll ".($roll + 1));
            $progress = $io->createProgressBar(100);
            for ($i = 0; $i < 10; $i++) {
                $progress->advance(10);
                $progress->display();
                sleep(1);
            }
            $progress->finish();

            $lastResult = str_replace("_", ", ", $result);
            $io->info($lastResult);
            sleep(1);
        }

        $saveMessage = new SaveConfig(
            $input->getOption('ConfigName'),
            $message->getRollerConfig()
        );

        $io->info("saving result : ${lastResult} + ${games} games");
        $this->bus->dispatch($saveMessage);
        if ($saveMessage->getStatus()->getStatus() !== Status::SUCCESS) {
            $io->error('Problem on saving new config file');

            return Command::FAILURE;
        }

        $io->success('Done');

        return Command::SUCCESS;
    }
}
