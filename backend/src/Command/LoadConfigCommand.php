<?php

declare(strict_types=1);

namespace App\Command;

use App\Core\ValueObjects\Status;
use App\Messenger\Messages\CreateEmptyConfig;
use App\Messenger\Messages\LoadConfig;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

class LoadConfigCommand extends Command
{
    protected static $defaultName = 'App:LoadConfig';
    protected static $defaultDescription = 'Load your config';

    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();
        $this->bus = $bus;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('ConfigName', 'C', InputOption::VALUE_REQUIRED, 'config name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $message = new LoadConfig(
            (!empty($input->getOption('ConfigName')) ? $input->getOption('ConfigName') : "")
        );

        $io->info("loading...");
        $this->bus->dispatch($message);

        if (($message->getStatus()->getStatus() !== Status::SUCCESS) || ($message->getRollerConfig() === null)) {
            $io->error('Can\'t load config');

            return Command::FAILURE;
        }

        $configTable = [];
        foreach ($message->getRollerConfig()->getCombinations() as $key => $value) {
            $configTable[] = ["players" => str_replace("_", ", ", $key), "count" => $value];
        }

        $io->table(['gracze', 'ilość gier'], $configTable);
        $io->success('Done');

        return Command::SUCCESS;
    }
}
