<?php

declare(strict_types=1);

namespace App\Command;

use App\Core\ValueObjects\Status;
use App\Messenger\Messages\CreateEmptyConfig;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

class MakeEmptyConfigCommand extends Command
{
    protected static $defaultName = 'App:CreateEmptyConfig';
    protected static $defaultDescription = 'Create empty config';

    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();
        $this->bus = $bus;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('Players', 'P', InputOption::VALUE_REQUIRED, 'set list of users, separate with ";"')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (empty($input->getOption('Players'))) {
            $io->error('You not define users!');

            return Command::FAILURE;
        }

        $players = explode(";", $input->getOption('Players'));
        if (count($players) < 4) {
            $io->error('You need to add minimum 4 players separated by ";"');

            return Command::FAILURE;
        }

        $message = new CreateEmptyConfig($players);

        $io->info("creating...");
        $this->bus->dispatch($message);

        if ($message->getStatus()->getStatus() !== Status::SUCCESS) {
            $io->error('Can\'t create new empty config');

            return Command::FAILURE;
        }

        print "Config name: ".$message->getConfigPath().PHP_EOL;

        $io->success('Done');

        return Command::SUCCESS;
    }
}
