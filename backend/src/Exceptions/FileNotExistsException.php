<?php


namespace App\Exceptions;


use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileNotExistsException extends FileException
{
    protected function __construct(string $class, string $message)
    {
        parent::__construct("${class} -> ${message}", 409);
    }

    public static function create(string $class, string $message): FileNotExistsException
    {
        return new self($class, $message);
    }
}