<?php


namespace App\Exceptions;


use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class BadDataException extends BadRequestException
{
    protected function __construct(string $class, string $message)
    {
        parent::__construct("${class} -> ${message}", 400);
    }

    public static function create(string $class, string $message): BadDataException
    {
        return new self($class, $message);
    }
}