export const setLoading = (state, payload) => (state.loading = payload)
export const setConfigName = (state, payload) => (state.configName = payload)
export const setToken = (state, payload) => (state.token = payload)
export const setRole = (state, payload) => (state.role = payload)
