export const getLoading = (state) => state.loading
export const getConfigName = (state) => state.configName
export const getToken = (state) => state.token
export const getRole = (state) => state.role
