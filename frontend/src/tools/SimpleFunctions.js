export const SimpleFunctions = {
  methods: {
    implode(separator, arr) {
      return arr.join([separator]);
    },
    sleep(second) {
      return new Promise(resolve => setTimeout(resolve, (second * 1000)));
    }
  }
}
