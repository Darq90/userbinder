
const routes = [
  {
    path: '/login',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'login', component: () => import('pages/LoginPage.vue')}
    ]
  },

  {
    path: '/',
    meta: { authRequired: true },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'teamSettings', component: () => import('pages/TeamsSettings.vue')},
      { path: 'probabilityDistribution', component: () => import('pages/ProbabilityDistribution.vue')},
      { path: 'roll', component: () => import('pages/Roll.vue')},
      { path: 'logout', component: () => import('pages/LogOutPage.vue')}
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
