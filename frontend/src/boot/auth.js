export default ({ router, store }) => {
  router.beforeEach((to, from, next) => {
    if (to.matched.some(route => route.meta.authRequired) === true) {
      if (
        store.state.main.configName === ""
        || store.state.main.configName === undefined
        // || store.state.main.token === ""
        // || store.state.main.token === undefined
        // || localStorage.getItem('token') === ""
        // || localStorage.getItem('token') === undefined
        )
       {
        next({
          name: 'login'
        })
      } else {
        next();
      }
    } else {
      next();
    }
  });
}
