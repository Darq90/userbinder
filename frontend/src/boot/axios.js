import Vue from 'vue'
import axios from 'axios'

// Vue.prototype.$axios = axios

const instance = axios.create({
  baseURL: process.env.API_URI
})

export default ({ router, store }) => {
  console.log(instance.baseURL);

  // instance.defaults.headers.common.Authorization = `Bearer ${store.state.main.token || localStorage.getItem('token')}`
  // instance.interceptors.response.use(undefined, function (err) {
  //   if (
  //     err?.response?.status === 401 ||
  //     err?.response?.data?.code === 401 ||
  //     err?.response?.data?.message === 'USER_NOT_FOUND'
  //   ) {
  //     localStorage.removeItem('token')
  //     store.commit('main/setToken', '')
  //     router.push('/init')
  //   }
  //   store.commit('main/setLoading', null)
  //   store.commit('main/setLoading', false)
  //
  //   if (err?.response?.data?.violations) {
  //     Notify.create({
  //       message: err.response.data.violations.map(violation => {
  //         return `${RESPONSE_MESSAGES[violation.message] ? RESPONSE_MESSAGES[violation.message] : violation.message}\n`
  //       }),
  //       type: 'negative'
  //     })
  //   } else if (err?.response?.status === 400 && err?.response?.data) {
  //     Notify.create({
  //       message: err.response.data['hydra:description'] ? err.response.data['hydra:description'] : err.response.data,
  //       type: 'negative'
  //     })
  //   }
  //   return Promise.reject(err)
  // })
  Vue.prototype.$axios = instance
  return instance
}
