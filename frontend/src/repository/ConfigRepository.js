import Repository from 'src/repository/Repository'

const resource = '/config'

export default {
  getConfig(config) {
    return Repository.get(`${resource}`+ '/' + config)
  }
}
