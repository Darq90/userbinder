import Repository from 'src/repository/Repository'

const resource = '/user'

export default {
  logIn(login, password) {
    let data = {
      login: login,
      password: password
    };

    return Repository.post(`${resource}`+ '/login', JSON.stringify(data))
  }
}
